function holaMundo() {
    console.log("Hola Mundo");
}

// holaMundo();
// console.log(holaMundo());

function sumar(num_1, num_2) {
    var esNumero_num_1 = typeof num_1 == "number"
    var esNumero_num_2 = typeof num_2 == "number"
    if (esNumero_num_1 && esNumero_num_2) {
        return num_1 + num_2;
    } else {
        console.log("Error");
    }
}

// console.log(sumar(3,"e"))
function sumarNumberosArreglo(numeros) {
    var parametroDistintoDeNumero = false;
    var resultado = 0;
    for (var i = 0; i < numeros.length; i++) {
        var esNumero = typeof numeros[i] == "number";
        if (!esNumero) {
            parametroDistintoDeNumero = true;
        } else {
            resultado = resultado + numeros[i];
        }
    }
    var respuesta = {
        noEsNumero: parametroDistintoDeNumero,
        resultado: resultado
    };
    return respuesta;
}

console.log("Como estas")
console.log(sumarNumberosArreglo([1,2,3,4,5]))

function cambiarMayusculas(nombre,funcion){
    return `Hola ${(funcion(nombre))}`;
}

console.log(cambiarMayusculas("Jefrii",convertirMayusculas))

function convertirMayusculas(texto){
    return texto.toUpperCase();
}

////////////////////////////////////////////

function cambiarMinusculas(nombre,funcion){
    return `Hola ${(funcion(nombre))}`;
}

console.log(cambiarMinusculas("Jefrii",convertirMinusculas))

function convertirMinusculas(texto){
    return texto.toLowerCase();
}

function agregarPuntoFinal(texto){
    return texto+".";
}

console.log(cambiarMayusculas(("un exelente dia"),agregarPuntoFinal));

function letraInicioPalabraMayuscula(texto){
    var letraInicioPalabraMayuscula=texto[0].toUpperCase();
    var restoPalabra=texto.slice(1,texto.length);
    return letraInicioPalabraMayuscula+restoPalabra;
}

console.log(letraInicioPalabraMayuscula("hola como estas"))
console.log(cambiarMayusculas("luis",letraInicioPalabraMayuscula))

console.log()